# RAE359
## Distributed Systems
## Student: Vitālijs Devjatovskis	|	151REB007

### Links:

[Safari](https://www.safaribooksonline.com/)

[Distance Education Centre](http://tsc.edx.lv/)

[Distributed Systems in One Lesson](https://www.safaribooksonline.com/library/view/distributed-systems-in/9781491924914/)


### Videos:

|Videolekcijas pārskats|
|---|
|[![Videolekcijas pārskats](http://img.youtube.com/vi/Gv_AO44jSzE/0.jpg)](https://www.youtube.com/watch?v=Gv_AO44jSzE&list=PLJYbwyDgsX0WPpMfAv8FihJRKdwuabJVZ&t=2s&index=1)|

|Mana distributīvā sistēma|
|---|
|[![Mana distributīvā sistēma](http://img.youtube.com/vi/QKB7FJI0lZ8/0.jpg)](https://www.youtube.com/watch?v=QKB7FJI0lZ8&list=PLJYbwyDgsX0WPpMfAv8FihJRKdwuabJVZ&t=1s&index=2)|
|[Source codes](../blob/master/source)|

|Atskats uz kursu "Komunikācijas distributīvās sistēmas"|
|---|
|[![Atskats uz kursu "Komunikācijas distributīvās sistēmas"](http://img.youtube.com/vi/pLt5qsNMhks/0.jpg)](https://www.youtube.com/watch?v=pLt5qsNMhks&list=PLJYbwyDgsX0WPpMfAv8FihJRKdwuabJVZ&index=3)|
|(Feat. [Ričards Kudojars](https://bitbucket.org/Kudojar/) & [Ņikita Krupeņins](https://bitbucket.org/151REB044/))|