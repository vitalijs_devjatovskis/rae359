#https://pymotw.com/2/socket/tcp.html
import socket
import datetime


IP = '127.0.0.1'
PORT = 8000
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((IP, PORT))

data = s.recv(BUFFER_SIZE)

if data != "time":
    print "ERR: Uknown request"
    errm = "Request uknown, closing connection\n"
    s.send(errm)
    s.close()
    print "Request received:\n", data

if data == "time":
    ans = "%s\n" % str(datetime.datetime.now())
    s.send(ans)
    print "Sending TIME to %s" % IP

data = s.recv(BUFFER_SIZE)
s.close()

