#https://pymotw.com/2/socket/tcp.html
import socket

#req = raw_input('Enter request: ')
req = "date"

IP = '127.0.0.1'
PORT = 8000
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((IP, PORT))

n = 3
while (n>0):

    s.listen(1)
    conn, addr = s.accept()
    print 'Connection address:', addr

    while 1:
        conn.send(req)
        data = conn.recv(BUFFER_SIZE)
        if not data:
            break
            print "Connection could not be established"

        print "Connection established successfully"

        f = open("log.txt","a")
        f.write(data)
        f.close()

        n = n - 1
    
    conn.close()

